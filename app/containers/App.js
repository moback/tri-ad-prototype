import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div>
        <h1>Herbnew</h1>
        <Link to="home">Home</Link>
      </div>
    );
  }
}
export default App;
