import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom';
import App from './components/App';
import Home from './components/Home';
import Listimages from './components/ListImages';
import './app.global.css';


 localStorage.clear();
ReactDOM.render(<HashRouter>

  <Switch>
      <Route exact path="/" component={App} />
      <Route path="/home" component={Home} />
      <Route path="/list" component={Listimages} />
  </Switch>
</HashRouter>, document.getElementById('root'))

