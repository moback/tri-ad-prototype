import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import style from './ListImages.css';
import twenty5 from '../assets/twenty5.png';
import twenty2 from '../assets/twenty2.png';
import twenty1 from '../assets/twenty1.png';
import twenty3 from '../assets/twenty3.png';
import twenty4 from '../assets/twenty4.png';
import assets23 from '../assets/assets23.png';
import asset22 from '../assets/asset22.png';
import asset21 from '../assets/asset21.png';
import asset18 from '../assets/asset18.png';
import asset19 from '../assets/asset19.png';
import asset20 from '../assets/asset20.png';
import asset24 from '../assets/asset24.png';
import asset25 from '../assets/asset25.png';
import dot from '../assets/dot.png';

import asset26 from '../assets/asset26.png';
import asset27 from '../assets/asset27.png';
import asset28 from '../assets/asset28.png';
import asset29 from '../assets/asset29.png';
import asset30 from '../assets/asset30.png';
import asset31 from '../assets/asset31.png';
import des from '../assets/des.png';
import dshc from '../assets/dshc.png';
import { desktopCapturer } from 'electron';
export default class RightSideContent extends Component {

 

  constructor(props) {
    super(props);
    this.state = {
      objectIcon: 1,
      adjustIcon: 1,
      imgHardware:des
    };
  }

  play=(img)=>{
    console.log(img)
    this.setState({
      imgHardware:img==des?dshc:des
    })
  }

  changeActive = (id, index) => {
    if (index == 1) {
      this.setState({
        objectIcon: id
      });
    } else {
      this.setState({
        adjustIcon: id
      });
    }
  };

  render() {
    const { objectIcon, adjustIcon, imgHardware } = this.state;
    const data1 = [
      { name: 'Align Objects', brand: '+' },
      { name: 'Distribute Objects', brand: '+' },
      { name: 'Scale', brand: '+' },
      { name: 'Opacity', brand: '+' },
      { name: 'Position', brand: '+' }
    ];
    const data2 = [
      { name: 'Distribute Objects', brand: '+' },
      { name: 'Scale', brand: '+' },
      { name: 'Position', brand: '+' }
    ];
    const data3 = [
      { name: 'Align Objects', brand: '+' },
      { name: 'Position', brand: '+' }
    ];
    const data4 = [
      { name: 'Scale', brand: '+' },
      { name: 'Opacity', brand: '+' }
    ];

    const content1 = data1.map((list, index) => (
      <div className={`row m-0 ${style.adjustList}`} key={index}>
        <span className="col-6 listT text-truncate p-0">{list.name}</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.fbrand}`}>
          {list.brand}
        </span>
      </div>
    ));
    const content2 = data2.map((list, index) => (
      <div className={`row m-0 ${style.adjustList}`} key={index}>
        <span className="col-6 listT text-truncate p-0">{list.name}</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.fbrand}`}>
          {list.brand}
        </span>
      </div>
    ));
    const content3 = data3.map((list, index) => (
      <div className={`row m-0 ${style.adjustList}`} key={index}>
        <span className="col-6 listT text-truncate p-0">{list.name}</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.fbrand}`}>
          {list.brand}
        </span>
      </div>
    ));
    const content4 = data4.map((list, index) => (
      <div className={`row m-0 ${style.adjustList}`} key={index}>
        <span className="col-6 listT text-truncate p-0">{list.name}</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.fbrand}`}>
          {list.brand}
        </span>
      </div>
    ));


    return (
      <div className="contentList col-12 p-0">
        <div className={`${style.toolsProperties}`}>
          <div className={style.tapIcon}>
            <div className={style.tapIcond}>
            <img src={assets23} className={`${style.iconTap} ${
                objectIcon == 1 ? style.active : ''
              }`}
              onClick={() => this.changeActive(1, 1)} alt="icons" />
            </div>
            <div className={style.tapIcond}>
            <img src={asset22} className={` ${style.iconTap} ${
                objectIcon == 2 ? style.active : ''
              }`}
              onClick={() => this.changeActive(2, 1)} alt="icons" />
            </div> 
            <div className={style.tapIcond}>
            <img src={asset21}
              className={`${style.iconTap} ${
                objectIcon == 3 ? style.active : ''
              }`}
              onClick={() => this.changeActive(3, 1)}
              alt="icons" />
            </div>           
            <div className={style.tapIcond}>
            <img src={asset20}
              className={` ${style.iconTap} ${
                objectIcon == 4 ? style.active : ''
              }`}
              onClick={() => this.changeActive(4, 1)}
              alt="icons" />
            </div>  
            <div className={style.tapIcond}>
            <img src={asset19}
              className={`${style.iconTap} ${
                objectIcon == 5 ? style.active : ''
              }`}
              onClick={() => this.changeActive(5, 1)}
              alt="icons" />
            </div> 
            <div className={style.tapIcond}>
            <img src={asset18}
              className={`${style.iconTap} ${
                objectIcon == 6 ? style.active : ''
              }`}
              onClick={() => this.changeActive(6, 1)}
              alt="icons" /> 
            </div> 
          
          </div>
          {objectIcon == 1?(
          <div className={` ${style.mediaT}`}>{content1}</div>
          ):objectIcon == 2?(
            <div className={` ${style.mediaT}`}>{content2}</div>
          ):objectIcon == 3?(
            <div className={`${style.mediaT}`}>{content3}</div>
          ):objectIcon == 4?(
            <div className={` ${style.mediaT}`}>{content4}</div>
          ):(
            <div className={`${style.mediaT}`}>
                <h6 className={`${style.mediadevice}`}>Device Preview <span className={`${style.ps}`}>+</span></h6> 
              <h6 className={`${style.mediaselect}`}  >
                <img src={imgHardware} alt=""  onClick={() => this.play(imgHardware)}/>                
              </h6> 
              {/* imgToggle onClick={() => this.play()} */}
            </div>
          )}
        </div>

        <div className={`toolsProperties col-12 p-0  ${style.toolProps}`}>
          <div className={`d-inline-block col-12 p-0 ${style.tapIconn}`}>
          <img src={asset24}
              className={`fa fa-bars ${style.iconTap} ${
                adjustIcon == 1 ? style.active : ''
              }`}
              onClick={() => this.changeActive(1, 2)}
              alt="icons" />  
             <img src={asset25}
              className={`fa fa-square ${style.iconTap} ${
                adjustIcon == 2 ? style.active : ''
              }`}
              onClick={() => this.changeActive(2, 2)}
              alt="icons" />  
          </div>
          {adjustIcon == 1 ? (
            <div className={`${style.mediaT}`}>
              <div className={style.iconAdjust}>
              <div className={`${style.dotIconn}`}><img src={dot } className={`${style.dotIcon}`} alt="icons" /></div>
              <div className={`${style.dotNextIcong}`}>
                <img src={asset28 } className={`${style.dotNextIconn}`} alt="icons" />Adjustment Layer
              </div>
              </div>
              <div className={style.iconAdjust}>
                <div className={`${style.dotIconn}`}><img src={dot } className={`${style.dotIcon}`} alt="icons" /></div>
              <div className={`${style.dotNextIcong}`}><img src={asset27 } className={`${style.dotNextIconn}`} alt="icons" />
                00a_rastersed-layer
              </div>
              </div>
              <div className={style.iconAdjust}>
              <div className={`${style.dotIconn}`}><img src={dot } className={`${style.dotIcon}`} alt="icons" /></div>
              <div className={` ${style.dotNextIcong}`}><img src={asset28 } className={`${style.dotNextIconn}`} alt="icons" />00b_folder-one
              </div>
              </div>
              <div className={style.iconAdjust}>
              <div className={`${style.dotIconn}`}><img src={dot } className={`${style.dotIcon}`} alt="icons" /></div>
              <div className={`${style.dotNextIcon}`}><img src={asset29 } className={`${style.dotNextIconn}`} alt="icons" />00c_vector-layer
              </div>
              </div>
            </div>
          ) : (
            <div className={`col-12 p-0 ${style.mediaT}`}>
              <div className={style.iconAdjust}>
                <i className="fa fa-paint-brush pr-2 text-white"></i>
                00a_rastersed-layer
              </div>
              <div className={style.iconAdjust}>
                <i className="fa fa-folder pr-2 text-white"></i>00b_folder-one
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
                          