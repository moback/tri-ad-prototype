import React, { Component,Fragment } from 'react';
import { Link } from 'react-router-dom';
import Header from './Header';
import moment from 'moment';
import imageAdd from '../assets/plusImage.png';
import FooterList from './FooterList';
import style from './Home.css';
import twenty5 from '../assets/twenty5.png';
import twenty2 from '../assets/twenty2.png';
import twenty1 from '../assets/twenty1.png';
import twenty3 from '../assets/twenty3.png';
import twenty4 from '../assets/twenty4.png';
import lexusdark from '../assets/lexusdark.png';


export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contentData: JSON.parse(localStorage.getItem('listData'))
    };
    if (!this.state.contentData) {
      this.props.history.push('');
    }
  }

  onSubmit(index, list?: any) {
    const newImage = {
      id: moment().unix(),
      img: '',
      title: '',
      subTitle: '',
      timestamp: moment().unix(),
      lastSeen: '2min ago',
      active:true,
      resizeImg: [],
      resizeRotateImg: []
    };


    const saveResize = this.state.contentData;
    const indexs = list?saveResize.data.findIndex(x => x.id == list.id):-1;
    if (indexs > -1) {
      saveResize.data.map(x => {
        x.active = false;
      });
      saveResize.data[indexs].timestamp = moment().unix();
      saveResize.data[indexs].active = true;
    }
    // console.log(saveResize)
    localStorage.setItem('listData', JSON.stringify(saveResize));

    const dataList = index == 'recent' ? list : newImage;
    localStorage.setItem('resizableImg', JSON.stringify(dataList));
    this.props.history.push('/list');
  }

  componentDidMount() {
    this.times();
  }

  times() {
    /** timestamp convert hours, minutes **/
    const { contentData } = this.state;
    contentData &&
      contentData.data.map(x => {
        const lastDate = x.timestamp;
        const now = moment().unix();
        var seconds = now - lastDate;
        seconds = Number(seconds);
        var d = Math.floor(seconds / (3600 * 24));
        var h = Math.floor((seconds % (3600 * 24)) / 3600);
        var m = Math.floor((seconds % 3600) / 60);
        var s = Math.floor(seconds % 60);
        var lastOpened =
          d > 0 ? d + ' day' : h > 0 ? h + ' hour' : m > 0 ? m + ' min' : s > 0 ? s + ' sec': '1 day';
        x.lastSeen = 'Last opened ' + lastOpened + ' ago';
      });
    this.setState({
      contentData:this.state.contentData
    });
  }

  render() {
    const { contentData } = this.state;
    if (contentData) {
      const dataList =
        contentData &&
        contentData.data.map(list => (
          <div
            className={`${style.contentBox}`}
            key={list.id}
            onClick={() => this.onSubmit('recent', list)}
          >
            <div className="boxContent overflow-hidden shadow p-0">
              <div className="col-12 p-0 imgContent">
                <img className={style.imgs} src={list.img} alt="lexus" />
              </div>
              <div className={` ${style.contentTitle} ${list.active?style.active:''}`}>
                {/* <div className={`text-capitalize ${style.maintitle}`}>
                  {list.title}
                </div> */}
                <img className={style.maintitle} src={list.title} alt="lexus" />
                <br></br>
                <img className={style.grayTitles} src={list.subTitle} alt="lexus" />
                {/* <div className={`ml-0 ${style.grayTitle}`}>{list.subTitle}</div> */}
                <div className={style.bottomTitle}>{list.lastSeen}</div>
              </div>
            </div>
          </div>
        ));

      return (
        <Fragment>
        <div className="col-12 p-0 py-3">
            <Header />
          </div>
        <div className={`allList ${style.mainContainer}`}>

          <div className="listOfContent">
            <div className={`${style.profileAndCarbrand}`}>
              <div className={`${style.carbrand}`}>
                <h2 className=" titleHead font-weight-normal">
                  <p className={`${style.linkstyle}`}><Link to="" ><img src={lexusdark} className={`${style.ld}`} alt="lexus"/></Link></p>
                </h2>
              </div>
              <div className={`${style.profileicon}`}>
                <div className={`${style.profileiconset}`}>
                <span className={`${style.bgCT} ${style.mT}`}>M</span>
                <span className={`${style.bgCT} ${style.pT}`}>P</span>
                <span className={`${style.bgCT} ${style.tT}`}>T</span>
                <span className={`${style.bgCT} ${style.sT}`}>S</span>
                </div>
                <span className={`plus text-muted ${style.plusAddImage}`}>+</span>
              </div>
            </div>
            <div className={`row py-3 ${style.reantAndRightmenu}`}>
              <div className="col-sm-6">
                <h6 className={`${style.grayTitle}`}>Recent</h6>
              </div>
              <div className={` ${style.rightMenualignments}`}>

              <img src={twenty1 } className={`${style.iconmenuR} ${style.rightMenu}`} alt="icons" />
              <img src={twenty2 } className={`${style.iconmenuR} ${style.rightMenu}`} alt="icons" />
              <img src={twenty4 } className={`${style.iconmenuR} ${style.rightMenu}`} alt="icons" />
              <img src={twenty3 } className={`${style.iconmenuR} ${style.rightMenu}`} alt="icons" />
              <img src={twenty5 } className={`${style.iconmenuR} ${style.rightMenu}`} alt="icons" />
              </div>
            </div>
            <div className={`${style.listImages}`}>
              {dataList}
              {
              // <div
              //   className={` ${style.contentBox}`}
              //   onClick={() => this.onSubmit(contentData.data.length)}
              // >
              //   <div
              //     className={`boxContent shadow position-realative p-0 ${style.addNewImg}`}
              //   >
              //     <div className="imgContent">
              //       <img
              //         className={style.imgs}
              //         src={imageAdd}
              //         alt="new image"
              //       />
              //     </div>
              //   </div>
              // </div>
      }
            </div>
          </div>
          <div className="footerContent col-12 ">
            <FooterList />
          </div>
        </div>
        </Fragment>
      );
    }
  }
}
