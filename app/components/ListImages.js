import React, { Fragment, Component } from 'react';
import { Link } from 'react-router-dom';
import * as html2canvas from 'html2canvas';
import Modal from 'react-awesome-modal';
import Resizer from 'react-image-file-resizer';
import moment from 'moment';
import ResizableContent from './ResizableContent';
import DropFile from './DropFile';
import RightSideContent from './RightSideContent';
import style from './ListImages.css';
import one from '../assets/one.png';
import two from '../assets/two.png';
import three from '../assets/three.png';
import five from '../assets/five.png';
import six from '../assets/six.png';
import ten from '../assets/ten.png';
import nine from '../assets/nine.png';
import eight from '../assets/eight.png';
import seven from '../assets/seven.png';
import navicon from '../assets/navicon.png';
import battery from '../assets/battery.png';
import search from '../assets/search.png';
import rightarrow from '../assets/rightarrow.png';
import minimise from '../assets/minimise.png';
import maximise from '../assets/maximise.png';
import close from '../assets/close.png';
import Icon1 from '../assets/Icon1.png';
import Icon2 from '../assets/Icon2.png';
import Icon3 from '../assets/Icon3.png';
import Icon4 from '../assets/Icon4.png';
import Icon5 from '../assets/Icon5.png';
import user from '../assets/user.png';
import projectname from '../assets/Projectname.png';
import projectnameh from '../assets/projectnameh.png';
import category from '../assets/Category.png';
import categoryh from '../assets/Categoryh.png';
import state from '../assets/State.png';
import stateh from '../assets/Stateh.png';
export default class Listimages extends Component {
  

  constructor(props) {
    super(props);
    this.state = {
      data: JSON.parse(localStorage.getItem('resizableImg')),
      visible: false,
      loader: false,
      hImage1:projectname,
      hImage2:category,
      hImage3:state,
    };
    // console.log('storage', JSON.parse(localStorage.getItem('resizableImg')))
    // console.log('this.state.data', this.state.data)
    if (!this.state.data) {
      this.props.history.push('');
    }
  }
  componentDidMount() {
    this.saveInterval();
    window.addEventListener("keydown",  (event)=> {
      if (event.defaultPrevented) {
        return; // Should do nothing if the default action has been cancelled
      }
      console.log(event.key);
      if(this.state.data.resizeRotateImg && this.state.data.resizeRotateImg.length > 0){
        if(event.key == "Delete" || event.key == "Backspace"){
          const indexRotate = this.state.data.resizeRotateImg.findIndex(
            y => y.border == true
          );
          // console.log(inde)
          if(indexRotate > -1){
            this.removeImg(indexRotate);
          }
        }
    }else{
      alert("select any one image")
    }
      // var handled = false;
      // if (event.key !== undefined) {
      //   // Handle the event with KeyboardEvent.key and set handled true.
      // } else if (event.keyIdentifier !== undefined) {
      //   // Handle the event with KeyboardEvent.keyIdentifier and set handled true.
      // } else if (event.keyCode !== undefined) {
      //   // Handle the event with KeyboardEvent.keyCode and set handled true.
      // }
      // if (handled) {
      //   // Suppress "double action" if event handled
      //   event.preventDefault();
      // }
     }, true);
  }


  componentWillUnmount() {
    clearInterval(this.timerInterval);

  }

  saveInterval() {
    this.timerInterval = setInterval(() => {
      if (this.state.data && this.state.data.resizeRotateImg.length > 0) {
        this.setState({
          loader: true
        });
        if (this.state.loader) {
          this.onSubmit();
          setTimeout(() => {
            this.setState({
              loader: false
            });
          }, 2000);
        }
      }
    }, 20000);
  }

  closeModal() {
    this.state.data.img = '';
    this.state.data.title = '';
    this.state.data.subTitle = '';
    this.setState({
      data: this.state.data,
      visible: false
    });
  }


  bigImg=(img, index)=>{
     console.log(index)
     if(index == 1){
        // this.state.hImage1 == img?projectnameh:img
        this.setState({
          hImage1:img == projectname?projectnameh:projectname
        })
     }else if(index == 2){
      // this.state.hImage2 == img?categoryh:img
      this.setState({
        hImage2:img == category?categoryh:category
      })

     }else{
      // this.state.hImage3 == img?stateh:img
      this.setState({
        hImage3:img == state?stateh:state
      })
     }

    //  this.setState({
    //   hImage1:this.state.hImage1,
    //   hImage2:this.state.hImage2,
    //   hImage3:this.state.hImage3
    //  })
  }

  snapShotImage = () => {
    // const input = document.getElementById('page');
    // html2canvas(input).then(canvas => {
    //   const imgData = canvas.toDataURL('image/png');
    //   this.state.data.img = imgData;
    // });
    // this.setState({
    //   data: this.state.data,
    //   visible: true
    // });
    this.onSubmit();
  };

  inputChange = e => {
    this.state.data[e.target.name] = e.target.value;
    this.setState({
      data: this.state.data
    });
  };

  onDropImage = files => {
    // files.map(file =>
    //   Object.assign(file, {
    //     preview: URL.createObjectURL(file)
    //   })
    // );
    // e.preventDefault();
    let file = files[0];
    var FileSize = files[0].size / 1024 / 1024;
    if (FileSize > 2) {
      Resizer.imageFileResizer(
        files[0],
        300,
        300,
        'JPEG',
        100,
        0,
        uri => {
          const fileImg = {
            title: file.name,
            image: uri
          };
          this.addNewImage(fileImg);
        },
        'base64'
      );
    } else {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        const fileImg = {
          title: file.name,
          image: reader.result
        };
        this.addNewImage(fileImg);
      };
    }
  };

  addNewImage(file) {
    const addImage = {
      id: moment().unix(),
      top: 125,
      left: 300,
      width: 324,
      height: 576,
      rotateAngle: 0,
      title: file.title,
      subTitle: 'Cluster',
      image: file.image,
      border:false
    };

    /** resize add, resize image list **/
    const indexResize = this.state.data.resizeImg.findIndex(
      x => x.image == file.image
    );
    if (indexResize == -1) {
      this.state.data.resizeImg = [...this.state.data.resizeImg, addImage];
      this.state.data.resizeRotateImg = [
        ...this.state.data.resizeRotateImg,
        addImage
      ];
      this.setState({
        data: this.state.data
      });
    } else {
      const indexRotate = this.state.data.resizeRotateImg.findIndex(
        x => x.image == file.image
      );
      if (indexRotate == -1) {
        this.state.data.resizeImg[indexResize].top = 120;
        this.state.data.resizeImg[indexResize].left = 300;
        this.state.data.resizeImg[indexResize].width = 324;
        this.state.data.resizeImg[indexResize].height = 576;
        this.state.data.resizeImg[indexResize].rotateAngle = 0;
        this.state.data.resizeRotateImg = [
          ...this.state.data.resizeRotateImg,
          this.state.data.resizeImg[indexResize]
        ];
        this.setState({
          data: this.state.data
        });
      } else {
        alert('Already image exists!');
      }
    }
  }

  rmImg=index=>{  
    console.log("border is highlighting");
    this.state.data.resizeRotateImg.map(x=>{
      console.log("inside false");
      x.border = false; 
    })    
    this.state.data.resizeRotateImg[index].border=true;
    this.setState({
      data: this.state.data     
    });
  //   document.addEventListener("keydown", (event)=> {
  //     if (event.key === "Delete") {
  //       // console.log(this.state)
  //       // const indexRotate = this.state.data.resizeRotateImg.findIndex(
  //       //   y => y.border == true
  //       // );
  //       // // console.log(inde)
  //       // if(indexRotate > -1){
  //       //   this.removeImg(index);
  //       // }
  //     }
  // });
 }


  removeImg = index => {
    this.state.data.resizeRotateImg.splice(index, 1);
    this.setState({
      data: this.state.data
      
    });
  };

  removeAllImg = (index, id) => {
    this.state.data.resizeImg.splice(index, 1);
    const indexs = this.state.data.resizeRotateImg.findIndex(x => x.id == id);
    if (index > -1) {
      this.state.data.resizeRotateImg.splice(indexs, 1);
    }
    this.setState({
      data: this.state.data
    });
  };

  onSubmit = () => {
    const saveResizeData = localStorage.getItem('listData');
    // console.log("saveResize", saveResize)
    if (saveResizeData) {
      const saveResize = JSON.parse(localStorage.getItem('listData'));
      saveResize.data.map(x => {
        x.active = false;
      });
      const index = saveResize.data.findIndex(x => x.id == this.state.data.id);
      if (index > -1) {
        saveResize.data[index] = this.state.data;
        saveResize.data[index].active = true;
      } else {
        saveResize.data = [...saveResize.data, this.state.data];
      }
      // console.log("saveResize", saveResize)
      localStorage.setItem('listData', JSON.stringify(saveResize));
      // localStorage.removeItem('resizableImg');
      // this.props.history.push('/home');
    } else {
      clearInterval(this.timerInterval);
      localStorage.clear();
      this.props.history.push('');
    }
  };

  render() {
    const { data, loader, hImage1, hImage2, hImage3 } = this.state;
    if (data) {
      const listItems =
        data &&
        data.resizeRotateImg.map((list, index) => (
          <ResizableContent
            data={this.state.data}
            key={list.id}
            id={list.id}
            top={list.top}
            left={list.left}
            width={list.width}
            height={list.height}
            rotateAngle={list.rotateAngle}
          >
            <img src={list.image} className={`${list.border ? 'activeborder' : 'inactiveborder'}`} alt={list.title}  onClick={() => this.rmImg(index)}/>
            <span className="closeIcon" onClick={() => this.removeImg(index)}>
              X
            </span>
          </ResizableContent>
        ));

      const listimgs =
        data &&
        data.resizeImg.map((list, index) => (
          <div className={`media ${style.border}`} key={list.id}>
            <img
              src={list.image}
              className={style.imgCircle}
              alt={list.title}
              onClick={() => this.addNewImage(list)}
            />
            <div className={`media-body col-10 p-0 ${style.mediaBody}`}>
              <div
                className={`text-truncate ${style.title}`}
                onClick={() => this.addNewImage(list)}
              >
                {list.title}
              </div>
              <span
                className={style.closeImg}
                onClick={() => this.removeAllImg(index, list.id)}
              >
                x
              </span>
            </div>
          </div>
        ));

      const datasubheader = [
        { name: '' },
        { name: '>' },
        { name: 'CATEGORY' },
        { name: '>' },
        { name: 'State' }
      ];
      const subheadercontent = datasubheader.map((list, index) => (
        <div className="" key={index}>
          <span className={`${style.navdata}`}>{list.name}</span>
        </div>
      ));
      const datan = [
        { name: 'FILE'},
        { name: 'EDIT'},
        { name: 'VIEW'},   
        { name: 'SELECT'},
        { name: 'WINDOW'},     
        { name: 'HELP'}
      ];
      const content = datan.map((list, index) => (
        <div className="" key={index}>
          <span className={`${style.navdata}`}>{list.name}</span>
        </div>
      ));
      return (
        <Fragment>

            <div className={`${style.navbar}`}>
                <div className={`${style.logonHoverEffect}`}>

                  <div className={`${style.imageHover}`}>
                  <img src={navicon} alt="icons" className={`${style.logoIcon}`}/>
                  </div>
                    
                    
                  <div className={`${style.navbarHover}`}>
                {/* <div className="">
                    <img src={navicon}alt="icons" />
                </div> */}
                <div className={`d-flex ${style.navlistHover}`}>
                    {content}
                </div>
                {/* <div className={`${style.rightnavdataHover}`}>
                {loader ? (
                <span className="saveDataT">
                  Saving...
                  <div
                    className={`spinner-border text-muted ${style.loaderSave}`}
                  ></div>
                </span>
              ) : (
                <Fragment></Fragment>
              )} */}
                {/* <img src={search} className={`${style.imagesearch}`} alt="icons" />
                <img src={battery} className={`${style.imagebattery}`} alt="icons" />
                <img src={minimise} className={`${style.imageminimise}`} alt="icons" />
                <img src={maximise} className={`${style.imagemaximise}`} alt="icons" />
                <img src={close} className={`${style.imageclose}`} alt="icons" /> */}
                {/* </div> */}
                <div className={`${style.rightnavdatadark}`}>
                
                </div>
            </div>

                </div>

                
                <div className={`${style.navlist}`}>
              <Link to="">
                <p className={`${style.navitems}`}><img className={`${style.navitemspn}`} src={hImage1} onMouseLeave={()=>this.bigImg(hImage1, 1)}  onMouseOver={()=>this.bigImg(hImage1, 1)} alt="lexus"/></p>
              </Link>
              <div className={`${style.rightArrow}`}>
                <img src={rightarrow} alt="icons" />
              </div>
              <Link to="/home">
                <p className={`${style.navitems}`}><img className={`${style.navitemsc}`} onMouseLeave={()=>this.bigImg(hImage2, 2)}  onMouseOver={()=>this.bigImg(hImage2, 2)} src={hImage2} alt="lexus"/></p>
              </Link>
              <div className={`${style.rightArrow}`}>
                <img src={rightarrow} alt="icons" />
              </div>
              <p className={`${style.navitems}`}><img className={`${style.navitemss}`} onMouseLeave={()=>this.bigImg(hImage3, 3)}  onMouseOver={()=>this.bigImg(hImage3, 3)} src={hImage3} alt="lexus"/></p>
            </div>

                
                <div className={`${style.rightnavdata}`}>
                {loader ? (
                <span className="saveDataT">
                  Saving...
                  <div
                    className={`spinner-border text-muted ${style.loaderSave}`}
                  ></div>
                </span>
              ) : (
                <Fragment></Fragment>
              )}
                <img src={search} className={`${style.imagesearch}`} alt="icons" />
                <img src={battery} className={`${style.imagebattery}`} alt="icons" />
                <img src={minimise} className={`${style.imageminimise}`} alt="icons" />
                <img src={maximise} className={`${style.imagemaximise}`} alt="icons" />
                <img src={close} className={`${style.imageclose}`} alt="icons" />
                </div>
                <div className={`${style.rightnavdatadark}`}>
                
                </div>
            </div>

          {/* <div className={`${style.navbar}`}>
            <div className="">
              <img src={navicon} alt="icons" />
            </div>

            <div className={`${style.navlist}`}>
              <Link to="">
                <p className={`${style.navitems}`}>PROJECT NAME</p>
              </Link>
              <div className={`${style.rightArrow}`}>
                <img src={rightarrow} alt="icons" />
              </div>
              <Link to="/home">
                <p className={`${style.navitems}`}>CATEGORYE</p>
              </Link>
              <div className={`${style.rightArrow}`}>
                <img src={rightarrow} alt="icons" />
              </div>
              <p className={`${style.navitems}`}>State</p>
            </div>

            <div className={`${style.rightnavdata}`}>
              {loader ? (
                <span className="saveDataT">
                  Saving...
                  <div
                    className={`spinner-border text-muted ${style.loaderSave}`}
                  ></div>
                </span>
              ) : (
                <Fragment></Fragment>
              )}
              <img
                src={search}
                className={`${style.imagesearch}`}
                alt="icons"
              />
              <img
                src={battery}
                className={`${style.imagebattery}`}
                alt="icons"
              />
                <img src={minimise} className={`${style.imageminimise}`} alt="icons" />
                <img src={maximise} className={`${style.imagemaximise}`} alt="icons" />
                <img src={close} className={`${style.imageclose}`} alt="icons" />
            </div>
          </div> */}

          <div
            className={`col-12 pl-5 pr-5 pt-3 pb-0 ${style.listofResizable}`}
          >
            {/* {data && data.title ? (
            <div
              className={`col-12 my-4 p-0 ml-md-5 pl-md-5 ${style.breadCrumT}`}
            >
              <span className={style.itemT}>
              <Link to="">Lexus LF-30</Link></span>
              <i className="fa fa-angle-right px-2" />
              <span className={style.itemT}>
              <Link to="/home">{data.title}</Link></span>
              <i className="fa fa-angle-right px-2" />
              <span className="itemT">{data.subTitle}</span>
            </div>
          ) : (
            <div
              className={`col-12 my-4 p-0 ml-md-5 pl-md-5 ${style.breadCrumT}`}
            >
              <span className={style.itemT}>
                <Link to="">Lexus LF-30</Link>
              </span>
            </div>
          )} */}
            <div
              className={`mainContent position-relative pl-md-1 ${style.mainCnt}`}
            >
              <div className={style.iconLists}>
                <img src={nine} className={`${style.iconnine}`} alt="icons" />

                <div className={`${style.tooltip}`}>
                  <img
                    src={eight}
                    className={`${style.iconeight}`}
                    alt="icons"
                  />
                  <span className={`${style.tooltiptext}`}>Zoom</span>
                </div>

                <div className={`${style.tooltip}`}>
                  <img
                    src={seven}
                    className={`${style.iconseven}`}
                    alt="icons"
                  />
                  <span className={`${style.tooltiptext}`}>Label Name</span>
                </div>
              </div>

              <div className={`${style.iconList} ${style.bottomIconList}`}>
                <img src={one} className={`${style.iconone}`} alt="icons" />
                <img src={two} className={`${style.icontwo}`} alt="icons" />
                <img src={three} className={`${style.iconthree}`} alt="icons" />
                <img src={five} className={`${style.iconfive}`} alt="icons" />
                <img src={six} className={`${style.iconsix}`} alt="icons" />
                <img src={ten} className={`${style.iconten}`} alt="icons" />
              </div>
              <div className={`${style.lstImg}`}>
                <div
                  id="page"
                  className={`col-12 p-0 ${style.listOfImages} ${
                    data && data.resizeRotateImg.length == 0 ? '' : 'dropTitle'
                  }`}
                >

                  <DropFile ondrop={this.onDropImage} />
                  {listItems}

                </div>
              </div>
              <div
                className={` scrollBar ${style.imgList}`}
              >
                <RightSideContent />
                <div className={`col-12 p-0 d-none ${style.tapIcon}`}>
                  <i
                    className={`fa fa-file-image ${style.iconTap} ${style.active}`}
                  />
                  {data && data.resizeImg.length > 0 ? (
                    listimgs
                  ) : (
                    <div className={`col-12 p-2 text-center ${style.border}`}>
                      no data!
                    </div>
                  )}
                </div>
                <div className={`${style.bottomicons}`}>
                    <img src={Icon1} className={style.Icon3iconone} alt="" />
                    <img src={Icon2} className={style.Icon3icontwo} alt="" />
                    <img src={Icon3} className={style.Icon3iconthree} alt="" />
                    <img src={Icon4} className={style.Icon3iconfour} alt="" />
                    <img src={Icon5} className={style.Icon3icofive} alt="" />
                </div>
              </div>
              {
                // <div className={`col-12 text-right p-3 ${style.btnSubmit}`}>
                //   <button
                //     type="button"
                //     className={
                //       data && data.resizeRotateImg.length == 0
                //         ? style.disableClass
                //         : style.btnClass
                //     }
                //     onClick={this.snapShotImage}
                //     disabled={data && data.resizeRotateImg.length == 0}
                //   >
                //     Save
                //   </button>
                //   <Modal
                //     visible={this.state.visible}
                //     width="400"
                //     effect="fadeInUp"
                //   >
                //     <div className="text-left col-12 p-3">
                //       <div className="row m-0 mb-3">
                //         <label className="col-md-4">Title:</label>
                //         <div className="col-md-8">
                //           <input
                //             type="text"
                //             name="title"
                //             value={data.title}
                //             className="form-control"
                //             onChange={this.inputChange}
                //           />
                //         </div>
                //       </div>
                //       <div className="row m-0 mb-3">
                //         <label className="col-md-4">Description:</label>
                //         <div className="col-md-8">
                //           <input
                //             type="text"
                //             name="subTitle"
                //             value={data.subTitle}
                //             className="form-control"
                //             onChange={this.inputChange}
                //           />
                //         </div>
                //       </div>
                //       <div className="col-12 mt-4 text-center submitT p-0">
                //         <button
                //           type="button"
                //           className="btn btn-light"
                //           onClick={() => this.closeModal()}
                //         >
                //           Close
                //         </button>
                //         <button
                //           type="button"
                //           className={
                //             data && data.title && data.subTitle
                //               ? style.btnClass
                //               : style.disableClass
                //           }
                //           onClick={this.onSubmit}
                //           disabled={data && !data.title && !data.subTitle}
                //         >
                //           Save
                //         </button>
                //       </div>
                //     </div>
                //   </Modal>
                // </div>
              }
            </div>
          </div>
        </Fragment>
      );
    }
  }
}
