import React, {Component, useEffect, useState, useCallback} from 'react';
import Dropzone from 'react-dropzone';



export default class DropFile extends Component{

  constructor(props) {
    super(props);
  }


  render() {
      return (

      <Dropzone onDrop={this.props.ondrop}>
        {({getRootProps, getInputProps}) => (
            <div {...getRootProps({className: 'dropzone'})}>
              <input {...getInputProps()} />
              <div className="dragDropT">
                {/* Drag or click an image, get a preview! */}
                </div>
            </div>
        )}
      </Dropzone>
    );
  }

}


