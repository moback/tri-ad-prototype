import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import style from './App.css';
import sminus from '../assets/sminus.png';
import bplus from '../assets/bplus.png';
export default class SideConetent extends Component {
  constructor(props){
    super(props)
  }

  render() {
    const data = [
     

      { name: '', brand: '-' }
    ];
    const datas = [
      { name: 'Multimedia' },
      { name: 'Heads-up Display' },
      { name: 'Stargate' },
      { name: 'Passenger Windows' }
    ];
    const content = data.map((list, index) => (
      <div className={`row m-0 ${style.titleList}`} key={index}>
        <span className={`col-6 listT text-truncate p-0 ${style.contentName}`}>Filename</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.contentName}`}>Lexus LF-30</span>
        <span className={`col-6 listT text-truncate p-0 ${style.contentName}`}>Document Type</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.contentName}`}>.car-os</span>

        <span className={`col-6 listT text-truncate p-0 ${style.contentName}`}>Date Created</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.contentName}`}>02/10/2019</span>

        <span className={`col-6 listT text-truncate p-0 ${style.contentName}`}>Date File Modified</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.contentName}`}>23/10/2019</span>

        <span className={`col-6 listT text-truncate p-0 ${style.contentName}`}>File Size</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.contentName}`}>1.2GB</span>

        <span className={`col-6 listT text-truncate p-0 ${style.contentName}`}>Dimensions</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.contentName}`}>4,480x1200</span>

        <span className={`col-6 listT text-truncate p-0 ${style.contentName}`}>Associated Files</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.contentName}`}><img
              className={style.sminus}
              src={sminus}
              alt="toyota"
            /></span>
      </div>
    ));
    const content1 = datas.map((list, index) => (
      <div className={`row m-0 ${style.mediaT} ${style.titleList}`} key={index}>
        <span className={`col-6 listT text-truncate p-0 ${style.leftListName}`}>{list.name}</span>
        <span className={`col-6 listT text-truncate p-0 text-right ${style.rightMinus}`}><img
              className={style.sminus}
              src={sminus}
              alt="toyota"
            /></span>
      </div>
    ));


    return (
      <div className={`p-0 m-0 ${style.contentList}`}>
        <div className={`col-12 ${style.title} ${style.SideT}`} onClick={this.props.onpress}><img className={style.plus} src={bplus} alt="toyota"/>New Project</div>
        <div className="fileProperties col-12 p-0 px-4">
          <div className={style.contents}>
          <h5 className={style.hTitle}>File Properties</h5>
          <div className={`row m-0 ${style.titleList}`}>
            <span className={`col-6 listT text-truncate p-0 ${style.instrumentCluster}`}>Instrument Cluster</span>
            <span className={`col-6 listT text-truncate p-0 text-right ${style.rightPlus}`}>+</span>
          </div>
          <div className={`col-12 p-0  ${style.contentData}`}>{content}</div>
          <div className={`col-12 p-0  ${style.contentOneData}`}>{content1}</div>
          </div>
        </div>
      </div>
    );
  }
}




