import React, { Component, Fragment } from 'react';
import { Route, Link } from 'react-router-dom';
import Header from './Header';
import SideConetent from './SideConetent';
import moment from 'moment';
import cluster from '../assets/cluster.png';
import lexus1 from '../assets/lexus1.png';
import lexus2 from '../assets/lexus2.png';
import lexus3 from '../assets/lexus3.png';
import LexusImg1 from '../assets/LexusImg1.png';
import LexusImg2 from '../assets/LexusImg2.png';
import LexusImg3 from '../assets/LexusImg3.png';
import LexusImg4 from '../assets/LexusImg4.png';
import LexusImg5 from '../assets/LexusImg5.png';
import style from './App.css';
import bminus from '../assets/bminus.png';
import lexus from '../assets/lexus.png';
import am from '../assets/am.png';
import pm from '../assets/pm.png';
import dst from '../assets/dst.png';
import hof from '../assets/hof.png';
import ldr from '../assets/ldr.png';
import ic from '../assets/ic.png';
import icc from '../assets/icc.png';
import icl from '../assets/icl.png';
export default class App extends Component {
  constructor(props) {    
    super(props);
    this.state = {
      image: cluster,
      title: 'Lexus LF-30',
      lastSeen: '1day ago',
      timestamp: moment().unix(),
      data: [
        {
          id: moment().unix()+1,
          img: LexusImg1,
          title: am,
          subTitle: icc,
          timestamp: moment().unix(),
          lastSeen: '2min ago..',
          active: true,
          resizeImg: [],
          resizeRotateImg: []
        },
        {
          id: moment().unix()+2,
          img: LexusImg2,
          title: pm,
          subTitle: icc,
          timestamp: moment().unix(),
          lastSeen: '2min ago',
          active: false,
          resizeImg: [],
          resizeRotateImg: []
        },
        {
          id: moment().unix()+3,
          img: LexusImg3,
          title: hof,
          subTitle: ic,
          timestamp: moment().unix(),
          lastSeen: '2min ago',
          active: false,
          resizeImg: [],
          resizeRotateImg: []
        },
        {
          id: moment().unix()+4,
          img: LexusImg4,
          title: ldr,
          subTitle: ic,
          timestamp: moment().unix(),
          lastSeen: '2min ago',
          active: false,
          resizeImg: [],
          resizeRotateImg: []
        },
        {
          id: moment().unix()+5,
          img: LexusImg5,
          title: dst,
          subTitle: icl,
          timestamp: moment().unix(),
          lastSeen: '2min ago',
          active: false,
          resizeImg: [],
          resizeRotateImg: []
        }
      ]
    };
    // localStorage.clear();
  }

  componentDidMount() {
    this.times();
  }

  times() {
    const saveData = localStorage.getItem('listData');
    const listSave = saveData ? JSON.parse(saveData) : this.state;
    /** timestamp convert hours, minutes **/
    const lastDate = listSave.timestamp;
    const now = moment().unix();
    var seconds = now - lastDate;
    seconds = Number(seconds);
    var d = Math.floor(seconds / (3600 * 24));
    var h = Math.floor((seconds % (3600 * 24)) / 3600);
    var m = Math.floor((seconds % 3600) / 60);
    var s = Math.floor(seconds % 60);
    var lastOpened =
      d > 0
        ? d + ' day'
        : h > 0
        ? h + ' hour'
        : m > 0
        ? m + ' min'
        : s > 0
        ? s + ' sec'
        : '5 sec';
    this.state.lastSeen = 'Last opened ' + lastOpened + ' ago';
    this.setState(this.state);
  }

  onSubmit = () => {
    const saveData = localStorage.getItem('listData');
    // console.log('saveData', saveData)
    const listSave = saveData ? JSON.parse(saveData) : this.state;
    listSave.timestamp = moment().unix();
    localStorage.setItem('listData', JSON.stringify(listSave));
    this.props.history.push('/home');
  };

  render() {
    return (
      <Fragment>
        <div className={`${style.mainCont}`}>
      <div className={`col-12 p-0 py-3 ${style.mainContnav}`}>
        <Header />
      </div>
      <div className={`mainProject  m-0 ${style.mainProj}`}>
        <div className={`p-0 ${style.headernext}`}>
          <SideConetent onpress={() => this.onSubmit()} />
        </div>
        <div className="mainContent p-0">
          <div
            className={`col-12 ${style.title} ${style.titleHeading}`}
            onClick={() => this.onSubmit()}
          >
            Open recent 
            <img className={style.customMinus} src={bminus} alt="lexus" />
           
          </div>
          <div className={`col-12 p-0 ${style.recentContent}`} onClick={() => this.onSubmit()}>
            <div className={`${style.imgHomeWrapper}`}>
            <img
              className={style.imgHome}
              src={this.state.image}
              alt={this.state.title}
            />
            {/* <div className={`${style.gradient}`}></div> */}
            </div>


            <div className={`${style.listOfContent}`}>
              <h2
                className={`titleHead font-weight-normal ${style.titleHead}`}
              >
                {/* {this.state.title} */}
                <img src={lexus} alt="lexus" />
              </h2>
              <h6 className={style.grayTitle}>{this.state.lastSeen}</h6>

              <div
                className={style.customArrow}
                onClick={() => this.onSubmit()}
              ></div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </Fragment>
    );
  }
}
