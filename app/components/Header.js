import React, { Component, Fragment } from 'react';
import style from './App.css';
import navicon from '../assets/navicon.png';
import user from '../assets/user.png';
import search from '../assets/search.png';
import minimise from '../assets/minimise.png';
import maximise from '../assets/maximise.png';
import close from '../assets/close.png';

import { App, BrowserWindow } from 'electron';

export default class Header extends Component {
  

    
    
    closeWindow=()=>{
        const remote = require('electron').remote;
        var window = remote.getCurrentWindow();
        console.log( "close working :) ", window.close() );
        window.close();
    }

    minimize=()=>{
        const remote = require('electron').remote;
        var window = remote.getCurrentWindow();
        console.log( "minimize working :) ", remote.getCurrentWindow());
        window.minimize();
    }

    maximize=()=>{
        const remote = require('electron').remote;
        var window = remote.getCurrentWindow();
        console.log( "minimize working :) ", remote.getCurrentWindow());
       if(!window.isMaximized()){
           window.maximize();
       }else{
           window.unmaximize();
       }
    }

    render(){
        const data = [
            { name: 'FILE'},
            { name: 'EDIT'},
            { name: 'VIEW'},   
            { name: 'SELECT'},
            { name: 'WINDOW'},     
            { name: 'HELP'}
          ];
          const content = data.map((list, index) => (
            <div className={`${style.navdataTop}`} key={index}>
              <span className={`${style.navdata}`}>{list.name}</span>
            </div>
          ));
        return(
            <Fragment>
            <div className={`${style.navbar}`}>
                <div className="">
                    <img src={navicon}alt="icons" />
                </div>
                <div className={`d-flex ${style.navlist}`}>
                    {content}
                </div>
                <div className={`${style.rightnavdata}`}>
                <img src={search} className={`${style.imagesearch}`} alt="icons" />
                <img src={user} className={`${style.imageuser}`} alt="icons" />
                <img src={minimise} className={`${style.imageminimise}`} alt="icons" onClick={() => this.minimize()} />
                <img src={maximise} className={`${style.imagemaximise}`} alt="icons" onClick={() => this.maximize()} />
                <img src={close} className={`${style.imageclose}`} alt="icons" onClick={() => this.closeWindow()} />
                </div>
                <div className={`${style.rightnavdatadark}`}>
                
                </div>
            </div>
            </Fragment>
        )
    }

}