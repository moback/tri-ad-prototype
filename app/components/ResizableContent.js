import React, { Fragment, useState } from 'react';
import ResizableRect from 'react-resizable-rotatable-draggable';

const ResizableContent = props => {
  const [width, setWidth] = useState(props.width);
  const [height, setHeight] = useState(props.height);
  const [top, setTop] = useState(props.top);
  const [left, setLeft] = useState(props.left);
  const [rotateAngle, setRotateAngle] = useState(props.rotateAngle);

  const contentStyle = {
    width: width - 10,
    height: height - 10,
    top: 5,
    left: 5,
    transform: `rotate(${rotateAngle}deg)`,
    position: 'absolute'
  };
  const iconStyle = {
    top: -12,
    left: 20,
    transform: `rotate(${rotateAngle}deg)`,
    position: 'absolute'
  };
  const divStyle = {
    width,
    height,
    top,
    left,
    transform: `rotate(${rotateAngle}deg)`,
    position: 'absolute'
  };

  React.useEffect(() => {
    const index = props.data.resizeRotateImg.findIndex(x => x.id === props.id);
    if (index > -1) {
      props.data.resizeRotateImg[index].width = width;
      props.data.resizeRotateImg[index].height = height;
      props.data.resizeRotateImg[index].top = top;
      props.data.resizeRotateImg[index].left = left;
      props.data.resizeRotateImg[index].rotateAngle = rotateAngle;
    }
  });

  const handleResize = (style, isShiftKey, type) => {
    let { top, left, width, height } = style;
    setWidth(Math.round(width));
    setHeight(Math.round(height));
    setTop(Math.round(top));
    setLeft(Math.round(left));
  };

  const handleRotate = rotateAngle => {
    setRotateAngle(rotateAngle);
  };

  const handleDrag = (deltaX, deltaY) => {
    setLeft(left + deltaX);
    setTop(top + deltaY);
  };


  return (
    <Fragment>
      <div className="imageResize" style={divStyle}>
        {React.cloneElement(props.children[0], { style: contentStyle })}
        {React.cloneElement(props.children[1], { style: iconStyle })}
        <ResizableRect
          top={top}
          rotatable
          left={left}
          aspectRatio
          minWidth={10}
          width={width}
          minHeight={10}
          height={height}
          onDrag={handleDrag}
          onRotate={handleRotate}
          onResize={handleResize}
          zoomable="nw, ne, se, sw"
          rotateAngle={rotateAngle}
        />
      </div>
    </Fragment>
  );
};

export default ResizableContent;
